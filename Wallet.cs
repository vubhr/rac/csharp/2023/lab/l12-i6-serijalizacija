﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.IO;

namespace Serijalizacija {
  public class Wallet {
    private List<CryptoCurrency> currencies;

    public Wallet() {
      currencies = new List<CryptoCurrency>();
    }

    public void AddCurrency(CryptoCurrency currency) {
      // ...
    }

    public IEnumerable<CryptoCurrency> GetCurrencies() {
      return
        from c in currencies
        orderby c.Name
        select c;
    }

    // https://learn.microsoft.com/en-us/dotnet/standard/serialization/system-text-json/how-to?pivots=dotnet-8-0
    public void Serialize() {
      // TODO:
    }

    public void Deserialize() {
      // TODO:
    }
  }
}
