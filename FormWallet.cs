namespace Serijalizacija {
  public partial class FormWallet : Form {
    public Wallet wallet;

    public FormWallet() {
      InitializeComponent();
      wallet = new Wallet();
    }

    private void btnAddCurrency_Click(object sender, EventArgs e) {
      FormCurrencyAdd formCurrencyAdd = new FormCurrencyAdd();
      if (formCurrencyAdd.ShowDialog() == DialogResult.OK) {
        CryptoCurrency? newCurrency = formCurrencyAdd.NewCurrency;
        if (newCurrency != null) {
          // dodavanje nove valute
        }
      }
    }
  }
}