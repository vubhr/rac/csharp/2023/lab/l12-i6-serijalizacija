﻿using System;

namespace Serijalizacija {
  public partial class FormCurrencyAdd : Form {
    public CryptoCurrency? NewCurrency { get; set; }
    public FormCurrencyAdd() {
      InitializeComponent();
    }

    private void btnAdd_Click(object sender, EventArgs e) {
      // stvoriti CryptoCurrency objekt na temelju textboxova
      // ako je sve u redu, postaviti DialogResult na OK:
      DialogResult = DialogResult.OK;
      // zatvoriti formu
      Close();
    }
  }
}
