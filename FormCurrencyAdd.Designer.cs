﻿namespace Serijalizacija {
  partial class FormCurrencyAdd {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      lblCurrencyName = new Label();
      tbCurrencyName = new TextBox();
      tbCurrencyShortName = new TextBox();
      lblCurrencyShortName = new Label();
      tbBalance = new TextBox();
      lblBalance = new Label();
      btnAdd = new Button();
      SuspendLayout();
      // 
      // lblCurrencyName
      // 
      lblCurrencyName.AutoSize = true;
      lblCurrencyName.Location = new Point(12, 9);
      lblCurrencyName.Name = "lblCurrencyName";
      lblCurrencyName.Size = new Size(74, 15);
      lblCurrencyName.TabIndex = 0;
      lblCurrencyName.Text = "Naziv valute:";
      // 
      // tbCurrencyName
      // 
      tbCurrencyName.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      tbCurrencyName.Location = new Point(12, 27);
      tbCurrencyName.Name = "tbCurrencyName";
      tbCurrencyName.Size = new Size(381, 23);
      tbCurrencyName.TabIndex = 1;
      // 
      // tbCurrencyShortName
      // 
      tbCurrencyShortName.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      tbCurrencyShortName.Location = new Point(12, 71);
      tbCurrencyShortName.Name = "tbCurrencyShortName";
      tbCurrencyShortName.Size = new Size(381, 23);
      tbCurrencyShortName.TabIndex = 3;
      // 
      // lblCurrencyShortName
      // 
      lblCurrencyShortName.AutoSize = true;
      lblCurrencyShortName.Location = new Point(12, 53);
      lblCurrencyShortName.Name = "lblCurrencyShortName";
      lblCurrencyShortName.Size = new Size(119, 15);
      lblCurrencyShortName.TabIndex = 2;
      lblCurrencyShortName.Text = "Skraćeni naziv valute:";
      // 
      // tbBalance
      // 
      tbBalance.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      tbBalance.Location = new Point(12, 115);
      tbBalance.Name = "tbBalance";
      tbBalance.Size = new Size(381, 23);
      tbBalance.TabIndex = 5;
      // 
      // lblBalance
      // 
      lblBalance.AutoSize = true;
      lblBalance.Location = new Point(12, 97);
      lblBalance.Name = "lblBalance";
      lblBalance.Size = new Size(42, 15);
      lblBalance.TabIndex = 4;
      lblBalance.Text = "Stanje:";
      // 
      // btnAdd
      // 
      btnAdd.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      btnAdd.DialogResult = DialogResult.OK;
      btnAdd.Location = new Point(12, 161);
      btnAdd.Name = "btnAdd";
      btnAdd.Size = new Size(381, 36);
      btnAdd.TabIndex = 6;
      btnAdd.Text = "Unesi";
      btnAdd.UseVisualStyleBackColor = true;
      btnAdd.Click += btnAdd_Click;
      // 
      // FormCurrencyAdd
      // 
      AutoScaleDimensions = new SizeF(7F, 15F);
      AutoScaleMode = AutoScaleMode.Font;
      ClientSize = new Size(405, 209);
      Controls.Add(btnAdd);
      Controls.Add(tbBalance);
      Controls.Add(lblBalance);
      Controls.Add(tbCurrencyShortName);
      Controls.Add(lblCurrencyShortName);
      Controls.Add(tbCurrencyName);
      Controls.Add(lblCurrencyName);
      Name = "FormCurrencyAdd";
      Text = "Unos valute";
      ResumeLayout(false);
      PerformLayout();
    }

    #endregion

    private Label lblCurrencyName;
    private TextBox tbCurrencyName;
    private TextBox tbCurrencyShortName;
    private Label lblCurrencyShortName;
    private TextBox tbBalance;
    private Label lblBalance;
    private Button btnAdd;
  }
}